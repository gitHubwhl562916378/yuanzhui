#include "yuanzhuirender.h"

void YuanZhuiRender::initsize(float h, float r, QImage &bottom, QImage &ce)
{
    program_.addCacheableShaderFromSourceFile(QOpenGLShader::Vertex,"vsrc.vert");
    program_.addCacheableShaderFromSourceFile(QOpenGLShader::Fragment,"fsrc.frag");
    program_.link();

    textureBottom_ = new QOpenGLTexture(bottom);
    textureCe_ = new QOpenGLTexture(ce);
    textureBottom_->setWrapMode(QOpenGLTexture::ClampToEdge);
    textureBottom_->setMinMagFilters(QOpenGLTexture::NearestMipMapNearest,QOpenGLTexture::LinearMipMapNearest);
    textureCe_->setWrapMode(QOpenGLTexture::ClampToEdge);
    textureCe_->setMinMagFilters(QOpenGLTexture::NearestMipMapNearest,QOpenGLTexture::LinearMipMapNearest);
    qreal angleSpan = 5;
    ceVert_ << 0 << h / 2 << 0;
    ceText_ << 0.5 << 1.0;
    ceNor_ << 0 << 1 << 0;
    bottomVert_ << 0 << -h/2 << 0;
    bottomText_ << 0.5 << 0.5;
    bottomNor_ << 0 << -1 << 0;
    for(qreal angle = 0; angle <= 360; angle += angleSpan){
        //侧面
        float curRad = angle * PI / 180;
        float x1 = r * ::cos(curRad);
        float y1 = - h/2;
        float z1 = r * ::sin(curRad);
        float tx1 = curRad / (2*PI);
        float ty1 = 0;

        ceVert_ << x1 << y1 << z1 ;
        ceText_ << tx1 << ty1;
        QVector3D norPlane = QVector3D::normal(QVector3D(0,h/2,0),QVector3D(0,-h/2,0),QVector3D(x1,y1,z1));
        QVector3D norP = QVector3D::crossProduct(norPlane,QVector3D(x1 - 0,y1 - h / 2,z1 -0));
        ceNor_ << norP.x() << norP.y() << norP.z();

        float bottomTx1 = 0.5 - 0.5 * ::cos(curRad);
        float bottomTy1 = 0.5 - 0.5 * ::sin(curRad);
        bottomVert_ << x1 << y1 << z1;
        bottomText_ << bottomTx1 << bottomTy1;
        bottomNor_ << 0 << -1 << 0;
    }
    QVector<GLfloat> vertVec;
    vertVec << ceVert_ << bottomVert_ << ceText_ << bottomText_ << ceNor_ << bottomNor_;
    vbo_.create();
    vbo_.bind();
    vbo_.allocate(vertVec.data(),vertVec.count() * sizeof(GLfloat));
}

void YuanZhuiRender::render(QOpenGLExtraFunctions *f, QMatrix4x4 &pMatrix, QMatrix4x4 &vMatrix, QMatrix4x4 &mMatrix, QVector3D &light, QVector3D &camera)
{
    f->glEnable(GL_DEPTH_TEST);

    program_.bind();
    vbo_.bind();
    f->glActiveTexture(GL_TEXTURE0);
    program_.setUniformValue("uPMatrix",pMatrix);
    program_.setUniformValue("uVMatrix",vMatrix);
    program_.setUniformValue("uMMatrix",mMatrix);
    program_.setUniformValue("uLightLocation",light);
    program_.setUniformValue("uCamera",camera);
    program_.setUniformValue("sTextures",0);

    program_.enableAttributeArray(0);
    program_.enableAttributeArray(1);
    program_.enableAttributeArray(2);
    program_.setAttributeBuffer(0,GL_FLOAT,0,3,3*sizeof(GLfloat));
    program_.setAttributeBuffer(1,GL_FLOAT,(ceVert_.count() + bottomVert_.count()) * sizeof(GLfloat),2,2*sizeof(GLfloat));
    program_.setAttributeBuffer(2,GL_FLOAT,(ceVert_.count() + bottomVert_.count() + ceText_.count() + bottomText_.count())*sizeof(GLfloat),3,3*sizeof(GLfloat));
    textureCe_->bind();
    f->glDrawArrays(GL_TRIANGLE_FAN,0,ceVert_.count() / 3);
    textureBottom_->bind();
    f->glDrawArrays(GL_TRIANGLE_FAN,ceVert_.count() / 3,bottomVert_.count() / 3);
    program_.disableAttributeArray(0);
    program_.disableAttributeArray(1);
    program_.disableAttributeArray(2);
    textureCe_->release();
    textureBottom_->release();
    vbo_.release();
    program_.release();

    f->glDisable(GL_DEPTH_TEST);
}
