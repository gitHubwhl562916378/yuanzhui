#ifndef YUANZHUIRENDER_H
#define YUANZHUIRENDER_H

#include <QOpenGLShaderProgram>
#include <QOpenGLBuffer>
#include <QOpenGLExtraFunctions>
#include <QOpenGLTexture>
#define PI 3.14159265f
class YuanZhuiRender
{
public:
    YuanZhuiRender() = default;
    void initsize(float h,float r,QImage &bottom,QImage &ce);
    void render(QOpenGLExtraFunctions *f,QMatrix4x4 &pMatrix,QMatrix4x4 &vMatrix,QMatrix4x4 &mMatrix,QVector3D &light,QVector3D &camera);

private:
    QOpenGLShaderProgram program_;
    QOpenGLBuffer vbo_;
    QVector<GLfloat> ceVert_,ceText_,ceNor_,bottomVert_,bottomText_,bottomNor_;
    QOpenGLTexture *textureCe_{nullptr},*textureBottom_{nullptr};
};

#endif // YUANZHUIRENDER_H
