#include "widget.h"

Widget::Widget(QWidget *parent)
    : QOpenGLWidget(parent)
{
    connect(&tm,SIGNAL(timeout()),this,SLOT(slotTimeout()));
    tm.start(30);
}

Widget::~Widget()
{

}

void Widget::initializeGL()
{
    render_.initsize(0.8,0.5,QImage("bottom.jpg"),QImage("rect.jpg"));
    light_.setX(15);
    light_.setY(3);
    light_.setZ(0);
    camera_.setX(0);
    camera_.setY(0);
    camera_.setZ(2);
}

void Widget::resizeGL(int w, int h)
{
    pMatrix.setToIdentity();
    pMatrix.perspective(45.0,float(w)/h,0.01f,200.0f);
}

void Widget::paintGL()
{
    QOpenGLExtraFunctions *f = QOpenGLContext::currentContext()->extraFunctions();
    f->glClearColor(0.0,0.0,0.0,1.0);
    f->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    QMatrix4x4 vMatrix;
    vMatrix.lookAt(camera_,QVector3D(0.0,0.0,0.0),QVector3D(0.0,1.0,0.0));

    QMatrix4x4 mMatrix;
    mMatrix.rotate(angleX,1,0,0);
    mMatrix.rotate(angleY,0,1,0);
    mMatrix.rotate(angleZ,0,0,1);
    render_.render(f,pMatrix,vMatrix,mMatrix,light_,camera_);
}

void Widget::mousePressEvent(QMouseEvent *event)
{
    Q_UNUSED(event)
    tm.stop();
}

void Widget::mouseReleaseEvent(QMouseEvent *event)
{
    Q_UNUSED(event)
    tm.start();
}

void Widget::slotTimeout()
{
    angleX += 5;
    angleY += 5;
    angleZ += 5;
    update();
}
