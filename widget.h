#ifndef WIDGET_H
#define WIDGET_H

#include <QOpenGLWidget>
#include <QTimer>
#include "yuanzhuirender.h"
class Widget : public QOpenGLWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = 0);
    ~Widget();

protected:
    void initializeGL() override;
    void resizeGL(int w,int h) override;
    void paintGL() override;
    void mousePressEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;

private:
    YuanZhuiRender render_;
    QTimer tm;
    QMatrix4x4 pMatrix;
    QVector3D light_,camera_;
    qreal angleX = 0,angleY = 0,angleZ = 0;

private slots:
    void slotTimeout();
};

#endif // WIDGET_H
